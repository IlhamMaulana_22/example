import java.io.*;
import java.util.Scanner;
import static java.lang.Math.sqrt;
class tugasMTK_team10 {
    //persegi
    public static void persegi()
    {
        Scanner input = new Scanner(System.in);

        double s,luas,keliling;
        System.out.println("\n-> Persegi <-");
        System.out.println("Masukkan Sisi : ");
        s=input.nextInt();
        System.out.println("=>");
        luas = s * s;
        keliling = 4*s;
        System.out.print("Luas = " + (int)luas + "\nKeliling  = "+ (int)keliling);
        System.out.println("");
    }

    //persegi panjang

    public static void p_panjang()
    {
        Scanner input = new Scanner(System.in);
        System.out.println("this is just experiment");
        double p,l,luas,keliling;
        System.out.println("\n–> Persegi Panjang <–");
        System.out.print("Masukkan Panjang : ");
        p = input.nextDouble();
        System.out.print("Masukkan Lebar : ");
        l = input.nextDouble();
        System.out.println("=>");
        luas = p*l;
        keliling =  2*(p + l);
        System.out.print("Luas  = " + (int)luas + "\nKeliling  = "+ (int)keliling);
        System.out.println("");
    }

    //lingkaran

    public static void lingkaran()
    {
        Scanner input = new Scanner(System.in);

        double phi = 3.14;
        double r, luas,keliling;

        System.out.println("Program Luas Lingkaran\n");
        System.out.print("Masukkan Panjang Jari-jari : ");
        r = input.nextDouble();

        luas = 0.5 * phi * r * r;
        keliling = 2 * phi * r;

        System.out.print("Luas Lingkaran = " + (int)luas + " \nKeliling lingkaran = "+ (int)keliling);

    }

    //segitiga

    public static void segitiga(){
        int a,t;
        double m, kll,luas;

        Scanner segitiga = new Scanner(System.in);
        System.out.print("Masukkan Alas: ");
        a = segitiga.nextInt();
        System.out.print("Masukkan Tinggi: ");
        t = segitiga.nextInt();
        System.out.println("");

        //menghitung luas
        luas = 0.5*a*t;
        System.out.println("Luasnya adalah: "+luas);
        //menghitung sisi miring
        m = sqrt((a*a)+(t*t));
        //menghitung keliling
        kll = a+t+m;
        System.out.println("Kelilingnya adalah: "+kll);

    }

    //trapesium

    public static void trapesium(){
        double rusuk1, rusuk2, tinggi;
        Scanner trapesium = new Scanner(System.in);
        System.out.println("masukkan panjang rusuk atas = ");
        rusuk1 = trapesium.nextDouble();
        System.out.println("masukkan panjang rusuk bawah = ");
        rusuk2 = trapesium.nextDouble();

        System.out.println("masukkan tinggi = ");
        tinggi = trapesium.nextDouble();
        double luas = 0.5 *(rusuk1 + rusuk2 )* tinggi;
        double keliling = rusuk1 + rusuk2+ tinggi *2;

        System.out.println("luas trapesium adalah = " + luas);
        System.out.println("keiling trapesium adalah = " + keliling);




        }
        //jajar genjang
        public static void jajargenjang() {
            //Membuat Instance/Objek BufferedReader
            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

            //Deklarasi Variable
            double alas, tinggi, sisi_miring, luas, keliling;



            try {
                //Memasukan Input
                System.out.print("Masukan Alas: ");
                alas = Double.parseDouble(input.readLine());
                System.out.print("Masukan Tinggi: ");
                tinggi = Double.parseDouble(input.readLine());
                System.out.print("Masukan Sisi Miring: ");
                sisi_miring = Double.parseDouble(input.readLine());
                //Menghitung Luas dan Keliling
                luas = alas * tinggi;
                keliling = 2 * (alas + sisi_miring);

                //Menampilkan Hasil
                System.out.println("Luas Jajar Genjang: " + luas);
                System.out.println("Keliling Jajar Genjang: " + keliling);
            } catch (IOException ex) {
                System.out.println("Terjadi Kesalahan");
            }
        }
        public static void belahketupat(){
        Scanner hello1 =new Scanner(System.in);

                double d1, d2,s, keliling, luas;




            System.out.println("masukkan nilai d1 : ");
            d1 = hello1.nextDouble();
            System.out.println("masukkan nilai d2 : ");
            d2 = hello1.nextDouble();
            System.out.println("masukkan nilai sisi");
            s = hello1.nextDouble();
            luas = 0.5 * d1 * d2 ;
            keliling = 4 * s;

            System.out.println("luas belah ketupat adalah : " + luas );
            System.out.println("keliling belah ketupat adalah : " + keliling );
        }
        public static void layanglayang(){
            Scanner nilai = new Scanner (System.in);
            double keliling , luas ;
            System.out.println("MENGHITUNG LUAS LAYANG-LAYANG");
            {
                int d1 , d2 ;
                System.out.println("Diagonal 1 = ");
                d1 = nilai.nextInt();
                System.out.println("Diagonal 2 = ");
                d2 = nilai.nextInt();
                luas = d1 * d2 / 2 ;
                System.out.println("Luas Layang-layang = "+luas);
            }
            System.out.println("\nMENGHITUNG KELILING LAYANG-LAYANG");
            {
                int sisiA , sisiB ;
                System.out.println("Sisi A = ");
                sisiA = nilai.nextInt();
                System.out.println("Sisi B = ");
                sisiB = nilai.nextInt();
                keliling = 2* sisiA + 2*sisiB;
                System.out.println("Keliling Layang-layang = "+keliling);
            }
            System.out.println("\nJadi Luas layang-layang = "+luas+ " dan Keliling = "+keliling);
        }
}






